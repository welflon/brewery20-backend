package ap.software.poland.brewery20.user

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @Throws(Exception::class)
    fun shouldSuccessGetAllUsers() {
        this.mockMvc.perform(
            MockMvcRequestBuilders.post("/brewery")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    """{
                        "query": "{getUsers{id login password email role}}",
                        "operationName": "query",
                        "variables": { "myVariable": "someValue" }
                    }"""
                )

        ).andExpect(MockMvcResultMatchers.status().isOk)
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.jsonPath("\$.errors").isEmpty)
            .andExpect(MockMvcResultMatchers.jsonPath("\$.data").isMap)
    }
}
