package ap.software.poland.brewery20.product

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath


@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @Throws(Exception::class)
    fun shouldSuccessGetAllProducts() {
        this.mockMvc.perform(
            MockMvcRequestBuilders.post("/brewery")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    """{
                          "query": "{getProducts{name id type}}",
                          "operationName": "query",
                          "variables": { "myVariable": "someValue" }
                        } """
                )

        ).andExpect(MockMvcResultMatchers.status().isOk)
            .andDo(print())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("\$.errors").isEmpty)
            .andExpect(jsonPath("\$.data").isMap)
    }

}
