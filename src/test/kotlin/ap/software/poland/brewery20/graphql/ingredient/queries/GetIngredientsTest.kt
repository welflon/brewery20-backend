package ap.software.poland.brewery20.graphql.ingredient.queries

import ap.software.poland.brewery20.ingredient.Ingredient
import ap.software.poland.brewery20.ingredient.IngredientService
import ap.software.poland.brewery20.product.Product
import ap.software.poland.brewery20.product.ProductType
import ap.software.poland.brewery20.recipe.Recipe
import ap.software.poland.brewery20.recipe.RecipeService
import graphql.schema.DataFetchingEnvironment
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks

internal class GetIngredientsTest {

    @Mock
    private lateinit var ingredientService: IngredientService

    @Mock
    private lateinit var recipeService: RecipeService

    @Mock
    private lateinit var dataFetcher: DataFetchingEnvironment

    @InjectMocks
    private lateinit var classUnderTest: GetIngredients

    private lateinit var exampleRecipe: Recipe
    private lateinit var exampleIngredient: Ingredient
    private lateinit var exampleProduct: Product

    companion object {
        private const val RECIPE_ID = 10
        private const val RECIPE_NAME = "Baltic Porter"
        private const val RECIPE_STYLE = "Porter"
        private const val RECIPE_WATER_IN_LITERS = 100.0
        private const val RECIPE_EBC = 70
        private const val RECIPE_IBU = 10
        private const val RECIPE_ALCOHOL = 12.0
        private const val RECIPE_BLG_AFTER_FILTRATION = 16.0
        private const val RECIPE_BLG_AFTER_FERMENTATION = 22.0
        private const val RECIPE_DESCRIPTION = "Secret beer of Poland"

        private const val INGREDIENT_ID = 11
        private const val INGREDIENT_QUANTITY = 11.0
        private const val INGREDIENT_UNIT = "kg"

        private const val PRODUCT_ID = 10
        private const val PRODUCT_NAME = "Vienna malt"
        private const val PRODUCT_QUANTITY = 100.0
        private const val PRODUCT_DESCRIPTION = "Example product to tests"
        private val PRODUCT_TYPE = ProductType.MALT
    }

    @BeforeEach
    private fun init() {
        openMocks(this)
        exampleRecipe = Recipe(
            RECIPE_ID,
            RECIPE_NAME,
            RECIPE_STYLE,
            RECIPE_WATER_IN_LITERS,
            RECIPE_EBC,
            RECIPE_IBU,
            RECIPE_ALCOHOL,
            RECIPE_BLG_AFTER_FILTRATION,
            RECIPE_BLG_AFTER_FERMENTATION,
            RECIPE_DESCRIPTION
        )

        exampleProduct = Product(
            PRODUCT_ID,
            PRODUCT_NAME,
            PRODUCT_TYPE,
            PRODUCT_QUANTITY,
            PRODUCT_DESCRIPTION,
            null
        )

        exampleIngredient =
            Ingredient(
                INGREDIENT_ID,
                exampleProduct,
                exampleRecipe,
                INGREDIENT_QUANTITY,
                INGREDIENT_UNIT
            )
    }

    private fun basicAssertion(ingredient: Ingredient) {
        assert(ingredient.id == exampleIngredient.id)
        assert(ingredient.quantity == exampleIngredient.quantity)
        assert(ingredient.unit == exampleIngredient.unit)
    }

    @Test
    fun getIngredients() {
        //given
        `when`(dataFetcher.getArgument<Int>(anyString())).thenReturn(1)
        `when`(recipeService.getById(anyInt())).thenReturn(exampleRecipe)
        `when`(ingredientService.getIngredientsByRecipeId(exampleRecipe)).thenReturn(listOf(exampleIngredient))
        //when
        val result = classUnderTest.get(dataFetcher)
        //then
        assert(result.isNotEmpty())
        basicAssertion(result.first())
        verify(dataFetcher, times(1)).getArgument<Int>(anyString())
        verify(recipeService, times(1)).getById(anyInt())
        verify(ingredientService, times(1)).getIngredientsByRecipeId(exampleRecipe)
    }
}
