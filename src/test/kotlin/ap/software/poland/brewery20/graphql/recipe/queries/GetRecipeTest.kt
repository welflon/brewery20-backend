package ap.software.poland.brewery20.graphql.recipe.queries

import ap.software.poland.brewery20.recipe.Recipe
import ap.software.poland.brewery20.recipe.RecipeService
import graphql.schema.DataFetchingEnvironment
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations.openMocks

internal class GetRecipeTest {
    @Mock
    private lateinit var recipeService: RecipeService

    @Mock
    private lateinit var dataFetcher: DataFetchingEnvironment

    @InjectMocks
    private lateinit var classUnderTest: GetRecipe

    private lateinit var exampleRecipe: Recipe

    companion object {
        private const val RECIPE_NAME = "Baltic Porter"
        private const val RECIPE_STYLE = "Porter"
        private const val RECIPE_WATER_IN_LITERS = 100.0
        private const val RECIPE_EBC = 70
        private const val RECIPE_IBU = 10
        private const val RECIPE_ALCOHOL = 12.0
        private const val RECIPE_BLG_AFTER_FILTRATION = 16.0
        private const val RECIPE_BLG_AFTER_FERMENTATION = 22.0
        private const val RECIPE_DESCRIPTION = "Secret beer of Poland"
    }

    @BeforeEach
    private fun init() {
        openMocks(this)
        exampleRecipe = Recipe(
            null,
            RECIPE_NAME,
            RECIPE_STYLE,
            RECIPE_WATER_IN_LITERS,
            RECIPE_EBC,
            RECIPE_IBU,
            RECIPE_ALCOHOL,
            RECIPE_BLG_AFTER_FILTRATION,
            RECIPE_BLG_AFTER_FERMENTATION,
            RECIPE_DESCRIPTION
        )
    }

    private fun basicAssertion(recipe: Recipe) {
        assert(recipe.name == exampleRecipe.name)
        assert(recipe.style == exampleRecipe.style)
        assert(recipe.waterInLiters == exampleRecipe.waterInLiters)
        assert(recipe.ebc == exampleRecipe.ebc)
        assert(recipe.ibu == exampleRecipe.ibu)
        assert(recipe.alcohol == exampleRecipe.alcohol)
        assert(recipe.blgAfterFiltration == exampleRecipe.blgAfterFiltration)
        assert(recipe.blgAfterFermentation == exampleRecipe.blgAfterFermentation)
        assert(recipe.description == exampleRecipe.description)
    }

    @Test
    fun getRecipe() {
        //given
        `when`(recipeService.getById(anyInt())).thenReturn(exampleRecipe)
        `when`(dataFetcher.getArgument<Int>(anyString())).thenReturn(1)
        //when
        val result = classUnderTest.get(dataFetcher)
        //then
        basicAssertion(result)
    }
}
