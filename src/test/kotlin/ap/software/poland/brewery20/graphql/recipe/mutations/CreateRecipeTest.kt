package ap.software.poland.brewery20.graphql.recipe.mutations

import ap.software.poland.brewery20.graphql.recipe.model.RecipeModel
import ap.software.poland.brewery20.recipe.Recipe
import ap.software.poland.brewery20.recipe.RecipeService
import graphql.schema.DataFetchingEnvironment
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyList
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks

internal class CreateRecipeTest {

    @Mock
    private lateinit var recipeService: RecipeService

    @Mock
    private lateinit var dataFetchingEnvironment: DataFetchingEnvironment

    @InjectMocks
    private lateinit var classUnderTest: CreateRecipe

    private lateinit var exampleRecipe: Recipe
    private lateinit var exampleRecipeModel: RecipeModel
    private val arguments = LinkedHashMap<String, Any>()
    private val inputData: LinkedHashMap<String, *> = LinkedHashMap(
        mapOf(
            "name" to RECIPE_NAME,
            "style" to RECIPE_STYLE,
            "waterInLiters" to RECIPE_WATER_IN_LITERS,
            "ebc" to RECIPE_EBC,
            "ibu" to RECIPE_IBU,
            "alcohol" to RECIPE_ALCOHOL,
            "blgAfterFiltration" to RECIPE_BLG_AFTER_FILTRATION,
            "blgAfterFermentation" to RECIPE_BLG_AFTER_FERMENTATION,
            "description" to RECIPE_DESCRIPTION,
            "ingredients" to ArrayList<String>()
        ) as LinkedHashMap<String, *>
    )

    companion object {
        private const val RECIPE_NAME = "Baltic Porter"
        private const val RECIPE_STYLE = "Porter"
        private const val RECIPE_WATER_IN_LITERS = 100.0
        private const val RECIPE_EBC = 70
        private const val RECIPE_IBU = 10
        private const val RECIPE_ALCOHOL = 12.0
        private const val RECIPE_BLG_AFTER_FILTRATION = 16.0
        private const val RECIPE_BLG_AFTER_FERMENTATION = 22.0
        private const val RECIPE_DESCRIPTION = "Secret beer of Poland"
    }

    @BeforeEach
    fun init() {
        openMocks(this)
        exampleRecipe = Recipe(
            null, RECIPE_NAME, RECIPE_STYLE, RECIPE_WATER_IN_LITERS, RECIPE_EBC, RECIPE_IBU,
            RECIPE_ALCOHOL, RECIPE_BLG_AFTER_FILTRATION, RECIPE_BLG_AFTER_FERMENTATION, RECIPE_DESCRIPTION
        )

        exampleRecipeModel = RecipeModel(
            mapOf(
                "name" to RECIPE_NAME,
                "style" to RECIPE_STYLE,
                "waterInLiters" to RECIPE_WATER_IN_LITERS,
                "ebc" to RECIPE_EBC,
                "ibu" to RECIPE_IBU,
                "alcohol" to RECIPE_ALCOHOL,
                "blgAfterFiltration" to RECIPE_BLG_AFTER_FILTRATION,
                "blgAfterFermentation" to RECIPE_BLG_AFTER_FERMENTATION,
                "description" to RECIPE_DESCRIPTION
            )
        )
    }

    private fun basicAssertion(recipe: Recipe) {
        assert(recipe.name == exampleRecipe.name)
        assert(recipe.style == exampleRecipe.style)
        assert(recipe.waterInLiters == exampleRecipe.waterInLiters)
        assert(recipe.ebc == exampleRecipe.ebc)
        assert(recipe.ibu == exampleRecipe.ibu)
        assert(recipe.alcohol == exampleRecipe.alcohol)
        assert(recipe.blgAfterFiltration == exampleRecipe.blgAfterFiltration)
        assert(recipe.blgAfterFermentation == exampleRecipe.blgAfterFermentation)
        assert(recipe.description == exampleRecipe.description)
    }

    @Test
    fun createRecipe() {
        //given
        arguments["recipe"] = inputData
        `when`(dataFetchingEnvironment.arguments).thenReturn(arguments)
        `when`(
            recipeService.createRecipe(any(RecipeModel::class.java) ?: exampleRecipeModel, anyList())
        ).thenReturn(exampleRecipe)

        //when
        val resultRecipe = classUnderTest.get(dataFetchingEnvironment)

        //then
        verify(recipeService, times(1)).createRecipe(
            any(RecipeModel::class.java) ?: exampleRecipeModel,
            anyList()
        )
        verify(dataFetchingEnvironment, times(1)).arguments
        basicAssertion(resultRecipe)
    }
}