package ap.software.poland.brewery20.graphql.recipe.mutations

import ap.software.poland.brewery20.recipe.RecipeService
import graphql.schema.DataFetchingEnvironment
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks

internal class DeleteRecipeTest {

    @Mock
    private lateinit var recipeService: RecipeService

    @Mock
    private lateinit var dataFetchingEnvironment: DataFetchingEnvironment

    @InjectMocks
    private lateinit var classUnderTest: DeleteRecipe

    companion object {
        private const val RECIPE_ID = 11
    }

    @BeforeEach
    fun init() {
        openMocks(this)
    }

    @Test
    fun deleteRecipe() {
        //given
        `when`(dataFetchingEnvironment.getArgument<Int>(anyString())).thenReturn(RECIPE_ID)
        //when
        classUnderTest.get(dataFetchingEnvironment)
        //then
        verify(recipeService, times(1)).deleteRecipe(RECIPE_ID)
    }

}