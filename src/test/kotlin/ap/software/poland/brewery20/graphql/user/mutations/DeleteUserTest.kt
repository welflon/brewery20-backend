package ap.software.poland.brewery20.graphql.user.mutations

import ap.software.poland.brewery20.user.UserService
import graphql.schema.DataFetchingEnvironment
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks

internal class DeleteUserTest {
    @Mock
    private lateinit var userService: UserService

    @Mock
    private lateinit var dataFetchingEnvironment: DataFetchingEnvironment

    @InjectMocks
    private lateinit var classUnderTest: DeleteUser

    companion object {
        private const val USER_ID = 11
    }

    @BeforeEach
    fun init() {
        openMocks(this)
    }

    @Test
    fun deleteProduct() {
        //given
        `when`(dataFetchingEnvironment.getArgument<Int>(anyString())).thenReturn(USER_ID)
        //when
        classUnderTest.get(dataFetchingEnvironment)
        //then
        verify(userService, times(1)).removeUserById(USER_ID)
    }
}
