package ap.software.poland.brewery20.graphql.product.mutations

import ap.software.poland.brewery20.graphql.product.model.ProductModel
import ap.software.poland.brewery20.product.Product
import ap.software.poland.brewery20.product.ProductService
import ap.software.poland.brewery20.product.ProductType
import graphql.schema.DataFetchingEnvironment
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks

internal class CreateProductTest {

    @Mock
    private lateinit var productService: ProductService

    @Mock
    private lateinit var dataFetchingEnvironment: DataFetchingEnvironment

    @InjectMocks
    private lateinit var classUnderTest: CreateProduct

    private lateinit var exampleProductModel: ProductModel
    private lateinit var exampleProduct: Product
    private val arguments = LinkedHashMap<String, Any>()
    private val inputData = LinkedHashMap(
        mapOf(
            "name" to PRODUCT_NAME,
            "type" to PRODUCT_TYPE.name,
            "quantity" to PRODUCT_QUANTITY,
            "description" to PRODUCT_DESCRIPTION
        ) as LinkedHashMap<String, *>
    )

    companion object {
        private const val PRODUCT_ID = 11
        private const val PRODUCT_NAME = "Vienna malt"
        private const val PRODUCT_QUANTITY = 100.0
        private const val PRODUCT_DESCRIPTION = "Example product to tests"
        private val PRODUCT_TYPE = ProductType.MALT
    }

    @BeforeEach
    fun init() {
        openMocks(this)
        exampleProductModel = ProductModel(inputData)
        exampleProduct = Product(
            PRODUCT_ID,
            PRODUCT_NAME,
            PRODUCT_TYPE,
            PRODUCT_QUANTITY,
            PRODUCT_DESCRIPTION,
            null
        )
    }

    private fun basicAssertion(product: Product) {
        assert(product.name == exampleProductModel.name)
        assert(product.type.name == exampleProductModel.type)
        assert(product.quantity == exampleProductModel.quantity)
        assert(product.description == exampleProductModel.description)
    }

    @Test
    fun createProduct() {
        //given
        inputData["id"] = PRODUCT_ID
        arguments["product"] = inputData
        `when`(dataFetchingEnvironment.arguments).thenReturn(arguments)
        `when`(productService.saveProduct(any(ProductModel::class.java) ?: exampleProductModel))
            .thenAnswer { exampleProduct }
        //when
        val resultProduct = classUnderTest.get(dataFetchingEnvironment)
        //then
        basicAssertion(resultProduct)
        verify(productService, times(1)).saveProduct(any(ProductModel::class.java) ?: exampleProductModel)
        verify(dataFetchingEnvironment, times(1)).arguments
    }
}
