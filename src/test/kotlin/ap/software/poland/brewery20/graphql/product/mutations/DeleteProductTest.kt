package ap.software.poland.brewery20.graphql.product.mutations

import ap.software.poland.brewery20.product.ProductService
import graphql.schema.DataFetchingEnvironment
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks

internal class DeleteProductTest {

    @Mock
    private lateinit var productService: ProductService

    @Mock
    private lateinit var dataFetchingEnvironment: DataFetchingEnvironment

    @InjectMocks
    private lateinit var classUnderTest: DeleteProduct

    companion object {
        private const val PRODUCT_ID = 11
    }

    @BeforeEach
    fun init() {
        openMocks(this)
    }

    @Test
    fun deleteProduct() {
        //given
        `when`(dataFetchingEnvironment.getArgument<Int>(ArgumentMatchers.anyString())).thenReturn(PRODUCT_ID)
        //when
        classUnderTest.get(dataFetchingEnvironment)
        //then
        verify(productService, times(1)).removeProductById(PRODUCT_ID)
    }
}
