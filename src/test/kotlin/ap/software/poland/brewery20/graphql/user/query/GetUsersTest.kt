package ap.software.poland.brewery20.graphql.user.query

import ap.software.poland.brewery20.user.User
import ap.software.poland.brewery20.user.UserRoles
import ap.software.poland.brewery20.user.UserService
import graphql.schema.DataFetchingEnvironment
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks

internal class GetUsersTest {
    @Mock
    private lateinit var userService: UserService

    @Mock
    private lateinit var dataFetcher: DataFetchingEnvironment

    @InjectMocks
    private lateinit var classUnderTest: GetUsers

    private lateinit var exampleUser: User

    companion object {
        private const val USER_ID = 10
        private const val USER_LOGIN = "Brewer20"
        private const val USER_PASSWORD = "password"
        private const val USER_EMAIL = "brewer20@wp.pl"
        private val USER_ROLE = UserRoles.BREWER
    }

    @BeforeEach
    private fun init() {
        openMocks(this)
        exampleUser = User(
            USER_ID,
            USER_LOGIN,
            USER_PASSWORD,
            USER_EMAIL,
            USER_ROLE
        )
    }

    private fun basicAssertion(user: User) {
        assert(user.id == exampleUser.id)
        assert(user.login == exampleUser.login)
        assert(user.password == exampleUser.password)
        assert(user.email == exampleUser.email)
        assert(user.role == exampleUser.role)
    }

    @Test
    fun getUsers() {
        //given
        `when`(userService.getAll()).thenReturn(listOf(exampleUser))
        //when
        val result = classUnderTest.get(dataFetcher)
        //then
        basicAssertion(result.first())
        verify(userService, times(1)).getAll()
    }
}
