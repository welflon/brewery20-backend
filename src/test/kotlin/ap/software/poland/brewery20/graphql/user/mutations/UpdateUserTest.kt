package ap.software.poland.brewery20.graphql.user.mutations

import ap.software.poland.brewery20.graphql.user.model.UserModel
import ap.software.poland.brewery20.user.User
import ap.software.poland.brewery20.user.UserRoles
import ap.software.poland.brewery20.user.UserService
import graphql.schema.DataFetchingEnvironment
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks

internal class UpdateUserTest {

    @Mock
    private lateinit var userService: UserService

    @Mock
    private lateinit var dataFetchingEnvironment: DataFetchingEnvironment

    @InjectMocks
    private lateinit var classUnderTest: UpdateUser

    private lateinit var exampleUserModel: UserModel
    private lateinit var exampleUser: User
    private val arguments = LinkedHashMap<String, Any>()
    private val inputData = LinkedHashMap(
        mapOf(
            "login" to USER_LOGIN,
            "role" to USER_ROLE.name,
            "password" to USER_PASSWORD,
            "email" to USER_EMAIL
        ) as LinkedHashMap<String, *>
    )

    companion object {
        private const val USER_ID = 10
        private const val USER_LOGIN = "Brewer20"
        private const val USER_PASSWORD = "password"
        private const val USER_EMAIL = "brewer20@wp.pl"
        private val USER_ROLE = UserRoles.BREWER
    }

    @BeforeEach
    fun init() {
        openMocks(this)
        exampleUserModel = UserModel(inputData)
        exampleUser = User(
            USER_ID,
            USER_LOGIN,
            USER_PASSWORD,
            USER_EMAIL,
            USER_ROLE
        )
    }

    private fun basicAssertion(user: User) {
        assert(user.id == exampleUser.id)
        assert(user.login == exampleUser.login)
        assert(user.password == exampleUser.password)
        assert(user.email == exampleUser.email)
        assert(user.role == exampleUser.role)
    }

    @Test
    fun updateUser() {
        //given
        inputData["id"] = USER_ID
        arguments["user"] = inputData
        `when`(dataFetchingEnvironment.arguments).thenReturn(arguments)
        `when`(userService.saveUser(any(UserModel::class.java) ?: exampleUserModel))
            .thenAnswer { exampleUser }
        //when
        val resultUser = classUnderTest.get(dataFetchingEnvironment)
        //then
        basicAssertion(resultUser)
        verify(userService, times(1)).saveUser(any(UserModel::class.java) ?: exampleUserModel)
        verify(dataFetchingEnvironment, times(1)).arguments
    }
}