package ap.software.poland.brewery20.graphql.product.queries

import ap.software.poland.brewery20.product.Product
import ap.software.poland.brewery20.product.ProductService
import ap.software.poland.brewery20.product.ProductType
import graphql.schema.DataFetchingEnvironment
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks

internal class GetProductTest {

    @Mock
    private lateinit var productService: ProductService

    @Mock
    private lateinit var dataFetcher: DataFetchingEnvironment

    @InjectMocks
    private lateinit var classUnderTest: GetProduct

    private lateinit var exampleProduct: Product

    companion object {
        private const val PRODUCT_NAME = "Vienna malt"
        private const val PRODUCT_QUANTITY = 100.0
        private const val PRODUCT_DESCRIPTION = "Example product to tests"
        private val PRODUCT_TYPE = ProductType.MALT
    }

    @BeforeEach
    private fun init() {
        openMocks(this)
        exampleProduct = Product(
            null,
            PRODUCT_NAME,
            PRODUCT_TYPE,
            PRODUCT_QUANTITY,
            PRODUCT_DESCRIPTION, null
        )
    }

    private fun basicAssertion(product: Product) {
        assert(product.name == exampleProduct.name)
        assert(product.type == exampleProduct.type)
        assert(product.quantity == exampleProduct.quantity)
    }

    @Test
    fun getProducts() {
        //given
        `when`(productService.getById(anyInt())).thenReturn(exampleProduct)
        `when`(dataFetcher.getArgument<Int>(anyString())).thenReturn(1)
        //when
        val result: Product = classUnderTest.get(dataFetcher)
        //then
        basicAssertion(result)
        verify(productService, times(1)).getById(anyInt())
        verify(dataFetcher, times(1)).getArgument<Int>(anyString())
    }

}