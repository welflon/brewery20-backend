package ap.software.poland.brewery20.product

import ap.software.poland.brewery20.graphql.product.model.ProductModel
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks
import java.util.*

internal class ProductServiceTest {

    @Mock
    lateinit var productRepository: ProductRepository

    @InjectMocks
    private lateinit var classUnderTest: ProductService

    private lateinit var exampleProduct: Product
    private lateinit var exampleProductModel: ProductModel

    companion object {
        private const val PRODUCT_ID = 10
        private const val PRODUCT_NAME = "Vienna malt"
        private const val PRODUCT_QUANTITY = 100.0
        private const val PRODUCT_DESCRIPTION = "Example product to tests"
        private val PRODUCT_TYPE = ProductType.MALT
    }

    @BeforeEach
    fun init() {
        openMocks(this)
        exampleProduct = Product(PRODUCT_ID, PRODUCT_NAME, PRODUCT_TYPE, PRODUCT_QUANTITY, PRODUCT_DESCRIPTION, null)

    }

    private fun basicAssertion(product: Product) {
        assert(product.id == PRODUCT_ID)
        assert(product.name == PRODUCT_NAME)
        assert(product.type == PRODUCT_TYPE)
        assert(product.quantity == PRODUCT_QUANTITY)
        assert(product.description == PRODUCT_DESCRIPTION)
    }

    @Test
    fun saveProduct() {
        //given
        exampleProductModel = ProductModel(
            mapOf<String, Any>(
                "id" to PRODUCT_ID,
                "name" to PRODUCT_NAME,
                "type" to PRODUCT_TYPE.name,
                "quantity" to PRODUCT_QUANTITY,
                "description" to PRODUCT_DESCRIPTION
            )
        )
        `when`(productRepository.save(exampleProduct)).thenReturn(exampleProduct)
        //when
        val resultProduct = classUnderTest.saveProduct(exampleProductModel)
        //then
        basicAssertion(resultProduct)
        verify(productRepository, times(1)).save(exampleProduct)
    }

    @Test
    fun saveProductWithoutAllData() {
        //given
        exampleProductModel = ProductModel(
            mapOf<String, Any>(
                "name" to PRODUCT_NAME,
                "type" to PRODUCT_TYPE.name,
                "quantity" to PRODUCT_QUANTITY,
                "description" to PRODUCT_DESCRIPTION
            )
        )
        `when`(productRepository.save(exampleProduct)).thenReturn(exampleProduct)
        //when
        val exception = assertThrows<NoSuchElementException> {
            classUnderTest.saveProduct(exampleProductModel)
        }
        //then
        assert(exception.message.equals("Key id is missing in the map."))
        verify(productRepository, times(0)).save(exampleProduct)
    }

    @Test
    fun getAllProducts() {
        //given
        `when`(productRepository.findAll()).thenReturn(listOf(exampleProduct))
        //when
        val resultsProducts: List<Product> = classUnderTest.getAll()
        //then
        assert(resultsProducts.isNotEmpty())
        basicAssertion(resultsProducts[0])
        verify(productRepository, times(1)).findAll()
    }

    @Test
    fun getAllProductsWhenAnyIsNotExist() {
        //given
        `when`(productRepository.findAll()).thenReturn(emptyList())
        //when
        val resultsProducts: List<Product> = classUnderTest.getAll()
        //then
        assert(resultsProducts.isEmpty())
        verify(productRepository, times(1)).findAll()
    }

    @Test
    fun getProductById() {
        //given
        `when`(productRepository.findById(PRODUCT_ID)).thenReturn(Optional.of(exampleProduct))
        //when
        val resultProduct = productRepository.findById(PRODUCT_ID)
        //then
        assert(resultProduct.isPresent)
        basicAssertion(resultProduct.get())
        verify(productRepository, times(1)).findById(PRODUCT_ID)
    }

    @Test
    fun getProductByIdWhenIsNotExist() {
        //given
        `when`(productRepository.findById(PRODUCT_ID)).thenReturn(Optional.empty())
        //when
        val resultProduct = productRepository.findById(PRODUCT_ID)
        //then
        assert(resultProduct.isEmpty)
        verify(productRepository, times(1)).findById(PRODUCT_ID)
    }
}
