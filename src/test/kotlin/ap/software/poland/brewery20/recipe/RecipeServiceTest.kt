package ap.software.poland.brewery20.recipe

import ap.software.poland.brewery20.exceptions.BreweryException
import ap.software.poland.brewery20.graphql.ingredient.model.IngredientModel
import ap.software.poland.brewery20.graphql.recipe.model.RecipeModel
import ap.software.poland.brewery20.ingredient.IngredientService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

internal class RecipeServiceTest {

    @Mock
    lateinit var recipeRepository: RecipeRepository

    @Mock
    lateinit var ingredientService: IngredientService

    @InjectMocks
    private lateinit var classUnderTest: RecipeService

    private lateinit var exampleRecipe: Recipe
    private lateinit var exampleRecipeModel: RecipeModel
    private lateinit var exampleIngredientModel: IngredientModel

    companion object {
        private const val RECIPE_ID = 10
        private const val RECIPE_NAME = "Baltic Porter"
        private const val RECIPE_STYLE = "Porter"
        private const val RECIPE_WATER_IN_LITERS = 100.0
        private const val RECIPE_EBC = 70
        private const val RECIPE_IBU = 10
        private const val RECIPE_ALCOHOL = 12.0
        private const val RECIPE_BLG_AFTER_FILTRATION = 16.0
        private const val RECIPE_BLG_AFTER_FERMENTATION = 22.0
        private const val RECIPE_DESCRIPTION = "Secret beer of Poland"

        private const val INGREDIENT_MODEL_QUANTITY = 11.0
        private const val INGREDIENT_MODEL_UNIT = "kg"
        private const val INGREDIENT_MODEL_PRODUCT_ID = 10
    }

    @BeforeEach
    fun init() {
        MockitoAnnotations.openMocks(this)
        exampleRecipe = Recipe(
            RECIPE_ID, RECIPE_NAME, RECIPE_STYLE, RECIPE_WATER_IN_LITERS, RECIPE_EBC, RECIPE_IBU,
            RECIPE_ALCOHOL, RECIPE_BLG_AFTER_FILTRATION, RECIPE_BLG_AFTER_FERMENTATION, RECIPE_DESCRIPTION
        )

        exampleRecipeModel = RecipeModel(
            mapOf(
                "id" to RECIPE_ID,
                "name" to RECIPE_NAME,
                "style" to RECIPE_STYLE,
                "waterInLiters" to RECIPE_WATER_IN_LITERS,
                "ebc" to RECIPE_EBC,
                "ibu" to RECIPE_IBU,
                "alcohol" to RECIPE_ALCOHOL,
                "blgAfterFiltration" to RECIPE_BLG_AFTER_FILTRATION,
                "blgAfterFermentation" to RECIPE_BLG_AFTER_FERMENTATION,
                "description" to RECIPE_DESCRIPTION
            )
        )

        exampleIngredientModel =
            IngredientModel(
                mapOf(
                    "productId" to INGREDIENT_MODEL_PRODUCT_ID,
                    "quantity" to INGREDIENT_MODEL_QUANTITY,
                    "unit" to INGREDIENT_MODEL_UNIT
                )
            )
    }

    private fun basicAssertion(recipe: Recipe) {
        assert(recipe.id == exampleRecipe.id)
        assert(recipe.name == exampleRecipe.name)
        assert(recipe.style == exampleRecipe.style)
        assert(recipe.waterInLiters == exampleRecipe.waterInLiters)
        assert(recipe.ebc == exampleRecipe.ebc)
        assert(recipe.ibu == exampleRecipe.ibu)
        assert(recipe.alcohol == exampleRecipe.alcohol)
        assert(recipe.blgAfterFiltration == exampleRecipe.blgAfterFiltration)
        assert(recipe.blgAfterFermentation == exampleRecipe.blgAfterFermentation)
        assert(recipe.description == exampleRecipe.description)
    }

    @Test
    fun saveRecipe() {
        //given
        Mockito.`when`(recipeRepository.save(exampleRecipe)).thenReturn(Optional.of(exampleRecipe))
        //when
        val resultRecipe = classUnderTest.saveRecipe(exampleRecipe)
        //then
        assert(resultRecipe.isPresent)
        basicAssertion(resultRecipe.get())
        verify(recipeRepository, times(1)).save(exampleRecipe)
    }

    @Test
    fun getAllRecipes() {
        //given
        Mockito.`when`(recipeRepository.findAll()).thenReturn(listOf(exampleRecipe))
        //when
        val resultRecipes = classUnderTest.getAll()
        //then
        assert(resultRecipes.isNotEmpty())
        basicAssertion(resultRecipes[0])
        verify(recipeRepository, times(1)).findAll()
    }

    @Test
    fun getAllRecipesWhenAnyIsNotExist() {
        //given
        Mockito.`when`(recipeRepository.findAll()).thenReturn(emptyList())
        //when
        val resultRecipes = classUnderTest.getAll()
        //then
        assert(resultRecipes.isEmpty())
        verify(recipeRepository, times(1)).findAll()
    }

    @Test
    fun getRecipeById() {
        //given
        Mockito.`when`(recipeRepository.findById(RECIPE_ID)).thenReturn(Optional.of(exampleRecipe))
        //when
        val resultRecipe = classUnderTest.getById(RECIPE_ID)
        //then
        basicAssertion(resultRecipe)
        verify(recipeRepository, times(1)).findById(RECIPE_ID)
    }

    @Test
    fun getRecipeByIdWhenIsNotExist() {
        //given
        Mockito.`when`(recipeRepository.findById(RECIPE_ID)).thenReturn(Optional.empty())
        //when
        val exception = assertThrows<BreweryException> {
            classUnderTest.getById(RECIPE_ID)
        }
        //then
        assert(exception.message.equals("Cannot get recipe."))
        verify(recipeRepository, times(1)).findById(RECIPE_ID)
    }

    @Test
    fun createRecipeFromModel() {
        //given
        Mockito.`when`(recipeRepository.save(exampleRecipe)).thenReturn(Optional.of(exampleRecipe))
        //when
        val resultRecipe = classUnderTest.createRecipe(exampleRecipeModel, emptyList())
        //then
        basicAssertion(resultRecipe)
        verify(recipeRepository, times(1)).save(exampleRecipe)
        verify(ingredientService, times(1)).saveIngredients(Mockito.anyList())
    }

    @Test
    fun createRecipeFromModelWithoutAllData() {
        //given
        Mockito.`when`(recipeRepository.save(exampleRecipe)).thenReturn(Optional.of(exampleRecipe))
        exampleRecipeModel = RecipeModel(
            mapOf(
                "name" to RECIPE_NAME,
                "style" to RECIPE_STYLE,
                "waterInLiters" to RECIPE_WATER_IN_LITERS,
                "ebc" to RECIPE_EBC,
                "ibu" to RECIPE_IBU,
                "alcohol" to RECIPE_ALCOHOL,
                "blgAfterFiltration" to RECIPE_BLG_AFTER_FILTRATION,
                "blgAfterFermentation" to RECIPE_BLG_AFTER_FERMENTATION,
                "description" to RECIPE_DESCRIPTION
            )
        )
        //when
        val exception = assertThrows<NoSuchElementException> {
            classUnderTest.createRecipe(exampleRecipeModel, emptyList())
        }

        //then
        assert(exception.message.equals("Key id is missing in the map."))
        verify(recipeRepository, times(0)).save(exampleRecipe)
        verify(ingredientService, times(0)).saveIngredients(Mockito.anyList())
    }
}
