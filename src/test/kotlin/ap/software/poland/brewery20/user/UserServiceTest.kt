package ap.software.poland.brewery20.user

import ap.software.poland.brewery20.exceptions.BreweryException
import ap.software.poland.brewery20.graphql.user.model.UserModel
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks
import java.util.*

internal class UserServiceTest {

    @Mock
    lateinit var userRepository: UserRepository

    @InjectMocks
    private lateinit var classUnderTest: UserService

    private lateinit var exampleUser: User
    private lateinit var exampleUserModel: UserModel

    companion object {
        private const val USER_ID = 10
        private const val USER_LOGIN = "Brewer20"
        private const val USER_PASSWORD = "password"
        private const val USER_EMAIL = "brewer20@wp.pl"
        private val USER_ROLE = UserRoles.BREWER
    }

    @BeforeEach
    fun init() {
        openMocks(this)
        exampleUser = User(USER_ID, USER_LOGIN, USER_PASSWORD, USER_EMAIL, USER_ROLE)
    }

    private fun basicAssertion(user: User) {
        assert(user.id == exampleUser.id)
        assert(user.login == exampleUser.login)
        assert(user.password == exampleUser.password)
        assert(user.email == exampleUser.email)
        assert(user.role == exampleUser.role)
    }

    @Test
    fun createUser() {
        //given
        exampleUserModel = UserModel(
            mapOf(
                "id" to USER_ID,
                "login" to USER_LOGIN,
                "password" to USER_PASSWORD,
                "email" to USER_EMAIL,
                "role" to USER_ROLE.name
            )
        )
        `when`(userRepository.save(exampleUser)).thenReturn(exampleUser)
        //when
        val userResponse = classUnderTest.saveUser(exampleUserModel)
        //then
        basicAssertion(userResponse)
        verify(userRepository, times(1)).save(exampleUser)
    }

    @Test
    fun createUserWithoutAllData() {
        //given
        exampleUserModel = UserModel(
            mapOf(
                "login" to USER_LOGIN,
                "password" to USER_PASSWORD,
                "email" to USER_EMAIL,
                "role" to USER_ROLE.name
            )
        )
        `when`(userRepository.save(exampleUser)).thenReturn(exampleUser)
        //when
        val exception = assertThrows<NoSuchElementException> {
            classUnderTest.saveUser(exampleUserModel)
        }
        //then
        assert(exception.message.equals("Key id is missing in the map."))
        verify(userRepository, times(0)).save(exampleUser)
    }

    @Test
    fun getUserById() {
        //given
        `when`(userRepository.findById(USER_ID)).thenReturn(Optional.of(exampleUser))
        //when
        val userResponse = classUnderTest.getById(USER_ID)
        //then
        basicAssertion(userResponse)
        verify(userRepository, times(1)).findById(USER_ID)
    }

    @Test
    fun getUserByIdWhenUserIsNotExist() {
        //given
        `when`(userRepository.findById(USER_ID)).thenReturn(Optional.empty())
        //when
        val exception = assertThrows<BreweryException> {
            classUnderTest.getById(USER_ID)
        }
        //then
        assert(exception.message.equals("Cannot get user."))
        verify(userRepository, times(1)).findById(USER_ID)
    }

    @Test
    fun getAllUsers() {
        //given
        `when`(userRepository.findAll()).thenReturn(listOf(exampleUser))
        //when
        val usersResponse = classUnderTest.getAll()
        //then
        basicAssertion(usersResponse.first())
        verify(userRepository, times(1)).findAll()
    }

    @Test
    fun getAllUsersWhenNeitherIsExist() {
        //given
        `when`(userRepository.findAll()).thenReturn(emptyList())
        //when
        val usersResponse = classUnderTest.getAll()
        //then
        assert(usersResponse.isEmpty())
        verify(userRepository, times(1)).findAll()
    }

    @Test
    fun deleteUser() {
        //given
        //when
        classUnderTest.removeUserById(USER_ID)
        //then
        verify(userRepository, times(1)).deleteById(USER_ID)
    }
}
