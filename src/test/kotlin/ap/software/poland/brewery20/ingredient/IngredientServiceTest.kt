package ap.software.poland.brewery20.ingredient

import ap.software.poland.brewery20.graphql.ingredient.model.IngredientModel
import ap.software.poland.brewery20.product.Product
import ap.software.poland.brewery20.product.ProductService
import ap.software.poland.brewery20.product.ProductType
import ap.software.poland.brewery20.recipe.Recipe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyCollection
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.openMocks

internal class IngredientServiceTest {

    @Mock
    lateinit var ingredientRepository: IngredientRepository

    @Mock
    lateinit var productService: ProductService

    @InjectMocks
    private lateinit var classUnderTest: IngredientService

    private lateinit var exampleIngredient: Ingredient
    private lateinit var exampleProduct: Product
    private lateinit var exampleRecipe: Recipe
    private lateinit var exampleIngredientModel: IngredientModel

    companion object {
        private const val INGREDIENT_ID = 11
        private const val INGREDIENT_QUANTITY = 11.0
        private const val INGREDIENT_UNIT = "kg"

        private const val PRODUCT_ID = 10
        private const val PRODUCT_NAME = "Vienna malt"
        private const val PRODUCT_QUANTITY = 100.0
        private const val PRODUCT_DESCRIPTION = "Example product to tests"
        private val PRODUCT_TYPE = ProductType.MALT

        private const val RECIPE_ID = 10
        private const val RECIPE_NAME = "Baltic Porter"
        private const val RECIPE_STYLE = "Porter"
        private const val RECIPE_WATER_IN_LITERS = 100.0
        private const val RECIPE_EBC = 70
        private const val RECIPE_IBU = 10
        private const val RECIPE_ALCOHOL = 12.0
        private const val RECIPE_BLG_AFTER_FILTRATION = 16.0
        private const val RECIPE_BLG_AFTER_FERMENTATION = 22.0
        private const val RECIPE_DESCRIPTION = "Secret beer of Poland"
    }

    @BeforeEach
    fun init() {
        openMocks(this)
        exampleProduct = Product(
            PRODUCT_ID,
            PRODUCT_NAME,
            PRODUCT_TYPE,
            PRODUCT_QUANTITY,
            PRODUCT_DESCRIPTION,
            null
        )

        exampleRecipe = Recipe(
            RECIPE_ID,
            RECIPE_NAME,
            RECIPE_STYLE,
            RECIPE_WATER_IN_LITERS,
            RECIPE_EBC,
            RECIPE_IBU,
            RECIPE_ALCOHOL,
            RECIPE_BLG_AFTER_FILTRATION,
            RECIPE_BLG_AFTER_FERMENTATION,
            RECIPE_DESCRIPTION
        )

        exampleIngredient =
            Ingredient(
                INGREDIENT_ID,
                exampleProduct,
                exampleRecipe,
                INGREDIENT_QUANTITY,
                INGREDIENT_UNIT
            )

        exampleIngredientModel =
            IngredientModel(
                mapOf(
                    "id" to INGREDIENT_ID,
                    "productId" to exampleProduct.id,
                    "quantity" to INGREDIENT_QUANTITY,
                    "unit" to INGREDIENT_UNIT
                )
            )
    }

    private fun basicAssertion(ingredient: Ingredient) {
        assert(ingredient.id == exampleIngredient.id)
        assert(ingredient.quantity == exampleIngredient.quantity)
        assert(ingredient.unit == exampleIngredient.unit)
    }

    @Test
    fun saveIngredients() {
        //given
        `when`(ingredientRepository.saveAll(anyCollection()))
            .thenReturn(listOf(exampleIngredient))
        //when
        val resultIngredients = classUnderTest.saveIngredients(listOf(exampleIngredient))
        //then
        assert(resultIngredients.isNotEmpty())
        basicAssertion(resultIngredients[0])
        verify(ingredientRepository, times(1)).saveAll(anyCollection())
    }

    @Test
    fun getIngredientsByRecipeId() {
        //given
        `when`(ingredientRepository.findByRecipe(exampleRecipe)).thenReturn(listOf(exampleIngredient))
        //when
        val resultIngredients = classUnderTest.getIngredientsByRecipeId(exampleIngredient.recipe)
        //then
        assert(resultIngredients.isNotEmpty())
        basicAssertion(resultIngredients[0])
        verify(ingredientRepository, times(1)).findByRecipe(exampleRecipe)
    }

    @Test
    fun getIngredientsByRecipeIdWhenRecipeHasNotIngredients() {
        //given
        `when`(ingredientRepository.findByRecipe(exampleRecipe)).thenReturn(emptyList())
        //when
        val resultIngredients = classUnderTest.getIngredientsByRecipeId(exampleIngredient.recipe)
        //then
        assert(resultIngredients.isEmpty())
        verify(ingredientRepository, times(1)).findByRecipe(exampleRecipe)
    }

    @Test
    fun createIngredientFromModel() {
        //given
        `when`(
            productService.getById(exampleIngredientModel.productId)
        ).thenReturn(exampleProduct)
        //when
        val resultIngredient = classUnderTest.createIngredientFromModel(exampleIngredientModel, exampleRecipe)
        //then
        assert(resultIngredient.recipe == exampleRecipe)
        assert(resultIngredient.quantity == exampleIngredient.quantity)
        assert(resultIngredient.unit == exampleIngredient.unit)
        verify(productService, times(1)).getById(exampleIngredientModel.productId)
    }

}