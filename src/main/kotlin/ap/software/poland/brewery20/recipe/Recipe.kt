package ap.software.poland.brewery20.recipe

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id


@Entity
data class Recipe(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "recipe_id")
    val id: Int?,
    val name: String,
    val style: String,
    val waterInLiters: Double,
    val ebc: Int,
    val ibu: Int,
    val alcohol: Double,
    val blgAfterFiltration: Double,
    val blgAfterFermentation: Double,
    val description: String
)
