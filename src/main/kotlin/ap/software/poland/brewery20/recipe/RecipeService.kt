package ap.software.poland.brewery20.recipe

import ap.software.poland.brewery20.exceptions.BreweryException
import ap.software.poland.brewery20.graphql.ingredient.model.IngredientModel
import ap.software.poland.brewery20.graphql.recipe.model.RecipeModel
import ap.software.poland.brewery20.ingredient.Ingredient
import ap.software.poland.brewery20.ingredient.IngredientService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import kotlin.streams.toList

@Service
class RecipeService(private val recipeRepository: RecipeRepository, private val ingredientService: IngredientService) {

    fun saveRecipe(recipe: Recipe): Optional<Recipe> {
        return recipeRepository.save(recipe)
    }

    fun getAll(): List<Recipe> {
        return recipeRepository.findAll()
    }

    fun getById(id: Int): Recipe {
        return recipeRepository.findById(id).orElseThrow {
            BreweryException("Cannot get recipe.")
        }
    }

    @Transactional
    fun createRecipe(recipeModel: RecipeModel, ingredientModels: List<IngredientModel>): Recipe {
        val recipe = prepareRecipeByModel(recipeModel)
        return saveRecipe(recipe)
            .map {
                ingredientService.saveIngredients(prepareIngredients(it, ingredientModels))
                it
            }
            .orElseThrow {
                throw BreweryException("Recipe cannot be modified")
            }
    }

    fun prepareIngredients(recipe: Recipe, ingredientModels: List<IngredientModel>): List<Ingredient> {
        return ingredientModels.stream()
            .map { ingredientService.createIngredientFromModel(it, recipe) }
            .toList()
    }

    fun deleteRecipe(id: Int): Any {
        return recipeRepository.deleteById(id)
    }

    fun prepareRecipeByModel(recipeModel: RecipeModel): Recipe {
        return Recipe(
            recipeModel.id,
            recipeModel.name,
            recipeModel.style,
            recipeModel.waterInLiters,
            recipeModel.ebc,
            recipeModel.ibu,
            recipeModel.alcohol,
            recipeModel.blgAfterFiltration,
            recipeModel.blgAfterFermentation,
            recipeModel.description
        )
    }
}
