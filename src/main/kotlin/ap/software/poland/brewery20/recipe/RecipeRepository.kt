package ap.software.poland.brewery20.recipe

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface RecipeRepository : JpaRepository<Recipe, Int> {
    fun save(recipe: Recipe): Optional<Recipe>
}
