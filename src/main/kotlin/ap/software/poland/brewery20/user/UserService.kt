package ap.software.poland.brewery20.user

import ap.software.poland.brewery20.exceptions.BreweryException
import ap.software.poland.brewery20.graphql.user.model.UserModel
import org.springframework.stereotype.Service

@Suppress("FunctionOnlyReturningConstant")
@Service
class UserService(val userRepository: UserRepository) {
    fun isAuthorizedUser(): Boolean {
        //TODO to implement Issue#5
        return true
    }

    fun saveUser(userModel: UserModel): User {
        return userRepository.save(createUserByModel(userModel))
    }

    fun getById(id: Int): User {
        return userRepository.findById(id).orElseThrow {
            BreweryException("Cannot get user.")
        }
    }

    fun getAll(): List<User> {
        return userRepository.findAll()
    }

    fun removeUserById(id: Int): Any {
        return userRepository.deleteById(id)
    }

    private fun createUserByModel(userModel: UserModel): User {
        return User(
            userModel.id,
            userModel.login,
            userModel.password,
            userModel.email,
            UserRoles.valueOf(userModel.role)
        )
    }
}
