package ap.software.poland.brewery20.user

enum class UserRoles {
    ADMIN,
    MANAGER,
    BREWER,
    WAREHOUSEMAN
}
