package ap.software.poland.brewery20.ingredient

import ap.software.poland.brewery20.recipe.Recipe
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface IngredientRepository : JpaRepository<Ingredient, Int> {
    override fun findById(id: Int): Optional<Ingredient?>
    fun findByRecipe(recipe: Recipe): List<Ingredient>
}
