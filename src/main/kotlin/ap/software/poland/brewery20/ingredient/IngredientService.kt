package ap.software.poland.brewery20.ingredient

import ap.software.poland.brewery20.graphql.ingredient.model.IngredientModel
import ap.software.poland.brewery20.product.ProductService
import ap.software.poland.brewery20.recipe.Recipe
import org.springframework.stereotype.Service

@Service
class IngredientService(
    private val ingredientRepository: IngredientRepository,
    private val productService: ProductService,
) {

    fun saveIngredients(ingredients: List<Ingredient>): List<Ingredient> {
        return ingredientRepository.saveAll(ingredients)
    }

    fun getIngredientsByRecipeId(recipe: Recipe): List<Ingredient> {
        return ingredientRepository.findByRecipe(recipe)
    }

    fun createIngredientFromModel(ingredientModel: IngredientModel, recipe: Recipe): Ingredient {
        val productFromDb = productService.getById(ingredientModel.productId)

        return Ingredient(
            ingredientModel.id,
            productFromDb,
            recipe,
            ingredientModel.quantity,
            ingredientModel.unit
        )
    }
}
