package ap.software.poland.brewery20.history

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface HistoryRepository : JpaRepository<History?, Long?> {
    override fun findById(id: Long): Optional<History?>
}
