package ap.software.poland.brewery20.history

import ap.software.poland.brewery20.recipe.Recipe
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Suppress("UnusedPrivateMember")
@Entity
data class History(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "history_id")
    private val id: Int,

    @Column(nullable = false)
    private val date: Date,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipe_id", nullable = true)
    private val recipe: Recipe,
    private val description: String?
)
