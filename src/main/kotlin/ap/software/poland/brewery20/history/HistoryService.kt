package ap.software.poland.brewery20.history

import org.springframework.stereotype.Service

@Service
class HistoryService(private val historyRepository: HistoryRepository) {
    //TODO to implement Issue#8
    fun saveHistory(history: History): History {
        return historyRepository.save(history)
    }

}
