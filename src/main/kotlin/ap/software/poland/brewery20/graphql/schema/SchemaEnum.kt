package ap.software.poland.brewery20.graphql.schema

enum class SchemaEnum(val path: String) {
    Main("/schema/main.graphql"),
    Products("/schema/products.graphql"),
    Recipes("/schema/recipes.graphql"),
    Ingredients("/schema/ingredients.graphql"),
    Users("/schema/users.graphql"),
}
