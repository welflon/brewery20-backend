package ap.software.poland.brewery20.graphql

import ap.software.poland.brewery20.user.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping(value = ["/brewery"])
@RestController
class GraphqlController(val graphQlService: GraphQlService, val userService: UserService) {

    @CrossOrigin
    @PostMapping
    fun getBreweryData(@RequestBody graphqlQueryModel: GraphqlQueryModel): ResponseEntity<Any> {
        return if (userService.isAuthorizedUser())
            ResponseEntity(graphQlService.graphQl.execute(graphqlQueryModel.query), HttpStatus.OK)
        else
            ResponseEntity(HttpStatus.UNAUTHORIZED)
        //TODO to adjust Issue#5
    }
}
