package ap.software.poland.brewery20.graphql.product.mutations

import ap.software.poland.brewery20.product.ProductService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class DeleteProduct(private val productService: ProductService) : DataFetcher<Any> {
    override fun get(environment: DataFetchingEnvironment): Any {
        return productService.removeProductById(environment.getArgument("id"))
    }

}
