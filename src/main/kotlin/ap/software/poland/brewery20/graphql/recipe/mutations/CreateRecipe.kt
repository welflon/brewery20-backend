package ap.software.poland.brewery20.graphql.recipe.mutations

import ap.software.poland.brewery20.graphql.ingredient.model.IngredientModel
import ap.software.poland.brewery20.graphql.recipe.model.RecipeModel
import ap.software.poland.brewery20.recipe.Recipe
import ap.software.poland.brewery20.recipe.RecipeService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component
import kotlin.streams.toList

@Suppress("UNCHECKED_CAST")
@Component
class CreateRecipe(private val recipeService: RecipeService) :
    DataFetcher<Recipe> {

    override operator fun get(environment: DataFetchingEnvironment): Recipe {
        val recipeInputMap: LinkedHashMap<String, *> =
            environment.arguments["recipe"] as LinkedHashMap<String, *>
        return recipeService.createRecipe(RecipeModel(recipeInputMap), createIngredientsModel(recipeInputMap))
    }

    private fun createIngredientsModel(recipeInputMap: LinkedHashMap<*, *>): List<IngredientModel> {
        val inputIngredients: ArrayList<LinkedHashMap<String, *>> =
            recipeInputMap["ingredients"] as ArrayList<LinkedHashMap<String, *>>
        return inputIngredients.stream()
            .map { IngredientModel(it) }
            .toList()
    }
}
