package ap.software.poland.brewery20.graphql.recipe.queries

import ap.software.poland.brewery20.recipe.Recipe
import ap.software.poland.brewery20.recipe.RecipeService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class GetRecipe(private val recipeService: RecipeService) : DataFetcher<Recipe> {

    override fun get(environment: DataFetchingEnvironment): Recipe {
        return getById(environment.getArgument("id"))
    }

    private fun getById(id: Int): Recipe {
        return recipeService.getById(id)
    }
}
