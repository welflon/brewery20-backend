package ap.software.poland.brewery20.graphql.recipe.model

data class RecipeModel(
    private val map: Map<String, Any?>
) {
    val id: Int? by map
    val name: String by map
    val style: String by map
    val waterInLiters: Double by map
    val ebc: Int by map
    val ibu: Int by map
    val alcohol: Double by map
    val blgAfterFiltration: Double by map
    val blgAfterFermentation: Double by map
    val description: String by map
}

