package ap.software.poland.brewery20.graphql.product.model


data class ProductModel(
    private val map: Map<String, Any?>
) {
    val id: Int? by map
    val name: String by map
    val type: String by map
    val quantity: Double by map
    val description: String? by map
}
