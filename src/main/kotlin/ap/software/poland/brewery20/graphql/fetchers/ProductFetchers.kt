package ap.software.poland.brewery20.graphql.fetchers

import ap.software.poland.brewery20.graphql.product.mutations.CreateProduct
import ap.software.poland.brewery20.graphql.product.mutations.DeleteProduct
import ap.software.poland.brewery20.graphql.product.mutations.UpdateProduct
import ap.software.poland.brewery20.graphql.product.queries.GetProduct
import ap.software.poland.brewery20.graphql.product.queries.GetProducts
import graphql.schema.idl.RuntimeWiring
import org.springframework.stereotype.Component

@Component
data class ProductFetchers(
    val productsDataFetcher: GetProducts,
    val productDataFetcher: GetProduct,
    val productCreateMutation: CreateProduct,
    val productUpdateMutation: UpdateProduct,
    val productDeleteMutation: DeleteProduct
) : Fetchers {

    override fun mutations(action: RuntimeWiring.Builder): RuntimeWiring.Builder {
        return action.type(ActionEnum.Mutation.name) { method ->
            method.dataFetcher("createProduct", productCreateMutation)
                .dataFetcher("updateProduct", productUpdateMutation)
                .dataFetcher("deleteProduct", productDeleteMutation)
        }
    }

    override fun queries(action: RuntimeWiring.Builder): RuntimeWiring.Builder {
        return action.type(ActionEnum.Query.name) { method ->
            method.dataFetcher("getProducts", productsDataFetcher)
                .dataFetcher("getProduct", productDataFetcher)
        }
    }
}
