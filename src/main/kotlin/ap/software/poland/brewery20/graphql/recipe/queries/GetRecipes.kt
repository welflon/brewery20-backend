package ap.software.poland.brewery20.graphql.recipe.queries

import ap.software.poland.brewery20.recipe.Recipe
import ap.software.poland.brewery20.recipe.RecipeService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class GetRecipes(private val recipeService: RecipeService) : DataFetcher<List<Recipe>> {

    override operator fun get(environment: DataFetchingEnvironment): List<Recipe> {
        return recipeService.getAll()
    }
}
