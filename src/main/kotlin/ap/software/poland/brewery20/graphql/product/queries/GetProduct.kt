package ap.software.poland.brewery20.graphql.product.queries

import ap.software.poland.brewery20.product.Product
import ap.software.poland.brewery20.product.ProductService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class GetProduct(private val productService: ProductService) : DataFetcher<Product> {

    override operator fun get(environment: DataFetchingEnvironment): Product {
        return getById(environment.getArgument("id"))
    }

    private fun getById(id: Int): Product {
        return productService.getById(id)
    }
}
