package ap.software.poland.brewery20.graphql.product.queries

import ap.software.poland.brewery20.product.Product
import ap.software.poland.brewery20.product.ProductService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class GetProducts(private val productService: ProductService) : DataFetcher<List<Product>> {

    override fun get(environment: DataFetchingEnvironment): List<Product> {
        return productService.getAll()
    }
}
