package ap.software.poland.brewery20.graphql.product.mutations

import ap.software.poland.brewery20.graphql.product.model.ProductModel
import ap.software.poland.brewery20.product.Product
import ap.software.poland.brewery20.product.ProductService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Suppress("UNCHECKED_CAST")
@Component
class UpdateProduct(private val productService: ProductService) : DataFetcher<Product> {
    override fun get(environment: DataFetchingEnvironment): Product {
        val productInputMap =
            environment.arguments["product"] as LinkedHashMap<String, *>
        return productService.saveProduct(ProductModel(productInputMap))
    }
}
