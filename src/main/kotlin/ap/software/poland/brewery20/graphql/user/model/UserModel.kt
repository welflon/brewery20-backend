package ap.software.poland.brewery20.graphql.user.model

class UserModel(
    private val map: Map<String, Any?>
) {
    val id: Int? by map
    val login: String by map
    val password: String by map
    val email: String by map
    val role: String by map
}
