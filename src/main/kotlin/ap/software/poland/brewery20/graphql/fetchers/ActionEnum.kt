package ap.software.poland.brewery20.graphql.fetchers

enum class ActionEnum {
    Query, Mutation
}
