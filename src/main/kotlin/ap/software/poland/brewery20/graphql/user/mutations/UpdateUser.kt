package ap.software.poland.brewery20.graphql.user.mutations

import ap.software.poland.brewery20.graphql.user.model.UserModel
import ap.software.poland.brewery20.user.User
import ap.software.poland.brewery20.user.UserService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Suppress("UNCHECKED_CAST")
@Component
class UpdateUser(val userService: UserService) : DataFetcher<User> {
    override fun get(environment: DataFetchingEnvironment): User {
        val userInputMap = environment.arguments["user"] as LinkedHashMap<String, *>
        return userService.saveUser(UserModel(userInputMap))
    }
}
