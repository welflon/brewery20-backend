package ap.software.poland.brewery20.graphql

import ap.software.poland.brewery20.graphql.fetchers.GraphqlFetcher
import ap.software.poland.brewery20.graphql.schema.SchemaEnum
import graphql.GraphQL
import graphql.schema.GraphQLSchema
import graphql.schema.idl.RuntimeWiring
import graphql.schema.idl.SchemaGenerator
import graphql.schema.idl.SchemaParser
import graphql.schema.idl.TypeDefinitionRegistry
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Service

@Service
class GraphQlService(
    graphqlFetcher: GraphqlFetcher
) {
    private val recipeDataFetchers = graphqlFetcher.recipeFetchers
    private val productDataFetchers = graphqlFetcher.productFetchers
    private val ingredientDataFetchers = graphqlFetcher.ingredientFetchers
    private val userDataFetchers = graphqlFetcher.userFetchers
    val graphQl = loadSchema()

    private fun loadSchema(): GraphQL {
        val typeRegistry: TypeDefinitionRegistry = SchemaParser().parse(getGraphqlSchema())
        val wiring: RuntimeWiring = buildRuntimeWiring()
        val schema: GraphQLSchema = SchemaGenerator().makeExecutableSchema(typeRegistry, wiring)
        return GraphQL.newGraphQL(schema).build()
    }

    private fun buildRuntimeWiring(): RuntimeWiring {
        val actions = RuntimeWiring.newRuntimeWiring()

        productDataFetchers.mutations(actions)
        productDataFetchers.queries(actions)
        ingredientDataFetchers.queries(actions)
        recipeDataFetchers.mutations(actions)
        recipeDataFetchers.queries(actions)
        userDataFetchers.mutations(actions)
        userDataFetchers.queries(actions)

        return actions.build()
    }

    private fun getGraphqlSchema(): String {
        var schema = ""
        SchemaEnum.values().forEach {
            schema += ClassPathResource(it.path).file.readText()
        }
        return schema
    }
}
