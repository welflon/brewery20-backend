package ap.software.poland.brewery20.graphql.user.query

import ap.software.poland.brewery20.user.User
import ap.software.poland.brewery20.user.UserService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class GetUsers(val userService: UserService) : DataFetcher<List<User>> {

    override fun get(environment: DataFetchingEnvironment): List<User> {
        return userService.getAll()
    }
}
