package ap.software.poland.brewery20.graphql.user.query

import ap.software.poland.brewery20.user.User
import ap.software.poland.brewery20.user.UserService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class GetUser(val userService: UserService) : DataFetcher<User> {

    override fun get(environment: DataFetchingEnvironment): User {
        return getById(environment.getArgument("id"))
    }

    private fun getById(id: Int): User {
        return userService.getById(id)
    }
}
