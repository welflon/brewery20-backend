package ap.software.poland.brewery20.graphql.fetchers

import ap.software.poland.brewery20.graphql.ingredient.queries.GetIngredients
import graphql.schema.idl.RuntimeWiring
import org.springframework.stereotype.Component

@Component
data class IngredientFetchers(val ingredientsDataFetcher: GetIngredients) : Fetchers {

    override fun mutations(action: RuntimeWiring.Builder): RuntimeWiring.Builder {
        return action
    }

    override fun queries(action: RuntimeWiring.Builder): RuntimeWiring.Builder {
        return action.type(ActionEnum.Query.name) { method ->
            method.dataFetcher("getIngredients", ingredientsDataFetcher)
        }
    }

}
