package ap.software.poland.brewery20.graphql.ingredient.queries

import ap.software.poland.brewery20.ingredient.Ingredient
import ap.software.poland.brewery20.ingredient.IngredientService
import ap.software.poland.brewery20.recipe.Recipe
import ap.software.poland.brewery20.recipe.RecipeService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class GetIngredients(
    private val ingredientService: IngredientService,
    private val recipeService: RecipeService
) : DataFetcher<List<Ingredient>> {

    override fun get(environment: DataFetchingEnvironment): List<Ingredient> {
        val recipeId = environment.getArgument("recipeId") as Int
        return getByRecipeId(recipeId)
    }

    private fun getByRecipeId(recipeId: Int): List<Ingredient> {
        val recipe: Recipe = recipeService.getById(recipeId)
        return ingredientService.getIngredientsByRecipeId(recipe)
    }
}
