package ap.software.poland.brewery20.graphql.recipe.mutations

import ap.software.poland.brewery20.recipe.RecipeService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class DeleteRecipe(private val recipeService: RecipeService) :
    DataFetcher<Any> {
    override fun get(environment: DataFetchingEnvironment): Any {
        return recipeService.deleteRecipe(environment.getArgument("id"))
    }
}
