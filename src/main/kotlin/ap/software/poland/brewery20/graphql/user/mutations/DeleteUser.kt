package ap.software.poland.brewery20.graphql.user.mutations

import ap.software.poland.brewery20.user.UserService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class DeleteUser(val userService: UserService) : DataFetcher<Any> {
    override fun get(environment: DataFetchingEnvironment): Any {
        return userService.removeUserById(environment.getArgument("id"))
    }
}
