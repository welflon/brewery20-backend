package ap.software.poland.brewery20.graphql.fetchers

import org.springframework.stereotype.Component

@Component
data class GraphqlFetcher(
    val recipeFetchers: RecipeFetchers,
    val productFetchers: ProductFetchers,
    val ingredientFetchers: IngredientFetchers,
    val userFetchers: UserFetchers
)
