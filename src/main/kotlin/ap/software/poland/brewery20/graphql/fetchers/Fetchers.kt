package ap.software.poland.brewery20.graphql.fetchers

import graphql.schema.idl.RuntimeWiring

interface Fetchers {
    fun mutations(action: RuntimeWiring.Builder): RuntimeWiring.Builder
    fun queries(action: RuntimeWiring.Builder): RuntimeWiring.Builder
}
