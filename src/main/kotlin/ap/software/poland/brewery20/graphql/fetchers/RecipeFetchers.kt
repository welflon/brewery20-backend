package ap.software.poland.brewery20.graphql.fetchers

import ap.software.poland.brewery20.graphql.recipe.mutations.CreateRecipe
import ap.software.poland.brewery20.graphql.recipe.mutations.DeleteRecipe
import ap.software.poland.brewery20.graphql.recipe.mutations.UpdateRecipe
import ap.software.poland.brewery20.graphql.recipe.queries.GetRecipe
import ap.software.poland.brewery20.graphql.recipe.queries.GetRecipes
import graphql.schema.idl.RuntimeWiring
import org.springframework.stereotype.Component

@Component
data class RecipeFetchers(
    val recipesDataFetcher: GetRecipes,
    val recipeDataFetcher: GetRecipe,
    val recipeCreateMutation: CreateRecipe,
    val recipeUpdateMutation: UpdateRecipe,
    val recipeDeleteMutation: DeleteRecipe
) : Fetchers {
    override fun mutations(action: RuntimeWiring.Builder): RuntimeWiring.Builder {
        return action.type(ActionEnum.Mutation.name) { method ->
            method.dataFetcher("createRecipe", recipeCreateMutation)
                .dataFetcher("updateRecipe", recipeUpdateMutation)
                .dataFetcher("deleteRecipe", recipeDeleteMutation)
        }
    }

    override fun queries(action: RuntimeWiring.Builder): RuntimeWiring.Builder {
        return action.type(ActionEnum.Query.name) { method ->
            method.dataFetcher("getRecipes", recipesDataFetcher)
                .dataFetcher("getRecipe", recipeDataFetcher)
        }
    }

}
