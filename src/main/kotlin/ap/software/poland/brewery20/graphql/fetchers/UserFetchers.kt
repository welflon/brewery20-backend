package ap.software.poland.brewery20.graphql.fetchers

import ap.software.poland.brewery20.graphql.user.mutations.CreateUser
import ap.software.poland.brewery20.graphql.user.mutations.DeleteUser
import ap.software.poland.brewery20.graphql.user.mutations.UpdateUser
import ap.software.poland.brewery20.graphql.user.query.GetUser
import ap.software.poland.brewery20.graphql.user.query.GetUsers
import graphql.schema.idl.RuntimeWiring
import org.springframework.stereotype.Component

@Component
data class UserFetchers(val usersDataFetcher: GetUsers,
                        val userDataFetcher: GetUser,
                        val userCreateMutation: CreateUser,
                        val userUpdateMutation: UpdateUser,
                        val userDeleteMutation: DeleteUser) : Fetchers {

    override fun mutations(action: RuntimeWiring.Builder): RuntimeWiring.Builder {
        return action.type(ActionEnum.Mutation.name) { method ->
            method.dataFetcher("createUser", userCreateMutation)
                .dataFetcher("updateUser", userUpdateMutation)
                .dataFetcher("deleteUser", userDeleteMutation)
        }
    }

    override fun queries(action: RuntimeWiring.Builder): RuntimeWiring.Builder {
        return action.type(ActionEnum.Query.name) { method ->
            method.dataFetcher("getUsers", usersDataFetcher)
                .dataFetcher("getUser", userDataFetcher)
        }
    }
}
