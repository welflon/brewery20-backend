package ap.software.poland.brewery20.graphql.ingredient.model

data class IngredientModel(
    private val map: Map<String, Any?>
) {
    val id: Int? by map
    val productId: Int by map
    val quantity: Double by map
    val unit: String by map
}
