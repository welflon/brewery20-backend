package ap.software.poland.brewery20.graphql

data class GraphqlQueryModel(
    val operationName: String?,
    val variables: HashMap<String, String>?,
    val query: String
)
