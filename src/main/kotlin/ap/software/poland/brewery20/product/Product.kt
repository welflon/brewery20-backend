package ap.software.poland.brewery20.product

import ap.software.poland.brewery20.ingredient.Ingredient
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
data class Product(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Int?,
    val name: String,
    val type: ProductType,
    var quantity: Double,
    val description: String?,

    @ManyToOne
    @JoinColumn(name = "ingredient_id", nullable = true)
    private val ingredient: Ingredient?
)
