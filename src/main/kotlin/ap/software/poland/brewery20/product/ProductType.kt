package ap.software.poland.brewery20.product

enum class ProductType {
    MALT, HOP, YEAST, SPECIAL
}
