package ap.software.poland.brewery20.product

import ap.software.poland.brewery20.exceptions.BreweryException
import ap.software.poland.brewery20.graphql.product.model.ProductModel
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun saveProduct(productModel: ProductModel): Product {
        return productRepository.save(createProductByModel(productModel))
    }

    fun getAll(): List<Product> {
        return productRepository.findAll()
    }

    fun getById(id: Int): Product {
        return productRepository.findById(id).orElseThrow {
            BreweryException("Cannot get product.")
        }
    }

    fun removeProductById(id: Int): Any {
        return productRepository.deleteById(id)
    }

    private fun createProductByModel(productModel: ProductModel): Product {
        return Product(
            productModel.id,
            productModel.name,
            ProductType.valueOf(productModel.type),
            productModel.quantity,
            productModel.description,
            null
        )
    }
}
