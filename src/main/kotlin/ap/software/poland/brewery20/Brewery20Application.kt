package ap.software.poland.brewery20

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Brewery20Application

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<Brewery20Application>(*args)
}
