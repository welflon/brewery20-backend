package ap.software.poland.brewery20.exceptions

class BreweryException(message: String) : Exception(message)
