
# Brewery 2.0 backend

_The best way to learn new things is do new things_
<br>
## 1.General:

###Definitions:

- Product
  - Basic element in warehouse
  - Exist 4 types of products:
    - HOPS
    - MALT
    - YEAST
    - SPECIAL
  - Provides detailed information about state in warehouse
- Recipe
  - Combination of products
  - Includes brewing dates as history
  - Provides detailed information about the finished beer
- Ingredient
  - Product include in recipe
- User
  - Exist 4 roles of users:
    - Admin 
    - Manager
    - Warehouseman
    - Brewer
 

Table of access and privileges in the system for rules:

| Role         | Create/Edit/Delete Users | Create/Edit/Delete Recipe | Read Recipe        | Create/Edit/Delete Ingredients | Read Ingredients   | 
|--------------|--------------------------|---------------------------|--------------------|--------------------------------|--------------------|
| ADMIN        | :heavy_check_mark:       | :heavy_check_mark:        | :heavy_check_mark: | :heavy_check_mark:             | :heavy_check_mark: |
| MANAGER      | :x:                      | :heavy_check_mark:        | :heavy_check_mark: | :x:                            | :heavy_check_mark: |
| WAREHOUSEMAN | :x:                      | :x:                       | :x:                | :heavy_check_mark:             | :heavy_check_mark: |
| BREWER       | :x:                      | :x:                       | :heavy_check_mark: | :x:                            | :heavy_check_mark: |


### Name convention:
- branch: B20b_NumberOfTask_Description
- commits: B20b_NumberOfTask Description
<br>

## 2.Checkstyle:

Detekt is one of the code quality verification tools.
Simply configure pom.xml file is enough to use detekt.
<br>
<br>Example configuration in Pom.xml:
```xml
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>1.8</version>
                <executions>
                    <execution>
                        <id>detekt</id>
                        <phase>verify</phase>
                        <configuration>
                            <target name="detekt">
                                <java taskname="detekt" dir="${basedir}"
                                      fork="true"
                                      failonerror="true"
                                      classname="io.gitlab.arturbosch.detekt.cli.Main"
                                      classpathref="maven.plugin.classpath">
                                    <arg value="--input"/>
                                    <arg value="${basedir}/src/main/kotlin"/>
                                    <arg value="--excludes"/>
                                    <arg value="**/special/package/internal/**"/>
                                    <arg value="--report"/>
                                    <arg value="xml:${basedir}/reports/detekt.xml"/>
                                </java>
                            </target>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>io.gitlab.arturbosch.detekt</groupId>
                        <artifactId>detekt-cli</artifactId>
                        <version>1.19.0-RC2</version>
                    </dependency>
                </dependencies>
            </plugin>

```
<B>*TIP ->*</B>
Annotation can be used to ignore a rule:
```kotlin
@Suppress("NameOfRule")
```
<br>To run detect with tests can be use option in maven menu: 
![maven](readme/mavenMenuVerify.png)

<br>or by terminal:
```shell
mvn verify
```


<br>result:
![result](readme/detectResult.png)



## 3.Graphql:

### Example structure of message to the graphql for that server:

```json
{
  "operationName" : "any text -> it is not necessary right now",
  "query": "graphql instruction as query or mutation",
  "variables": "should be empty right now"
}
```

<br>

operationName -> doesn't mean //TODO react required

content -> graphql instructions as query or mutation

variables -> doesnt mean // TODO react required

<br>

Example request body for query:

### Query
```json
{
  "query": "{getRecipes{id name style}}",
  "operationName": "query",
  "variables": { "myVariable": "someValue" }
}
```
```json
{
  "query": "{getIngredients(recipeId:70){product{name type}, quantity}}",
  "operationName": "query",
  "variables": { "myVariable": "someValue" }
}
```


### Mutation:
```json
{
  "query": "mutation{createProduct(product: {id:null, name: \"example124\", type: HOP, quantity: 118.1, description:\"\"})}",
  "operationName": "Mutation",
  "variables": { "myVariable": "someValue" }
}
```

```json
{
  "query": "mutation{createRecipe(recipe: {id:null,name: \"Baltic Porter\", style: \"\", waterInLiters:0, ebc:0, ibu:0, alcohol:0, blgAfterFiltration:0, blgAfterFermentation:0, description:\"\", ingredients: [{id:null,productId:137, quantity:0.2, unit:\"kg\"}, {id:null,productId:138, quantity:0.1, unit: \"kg\"}] })}",
  "operationName": "Mutation",
  "variables": { "myVariable": "someValue" }
}
```
